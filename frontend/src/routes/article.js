import ArticleList from '../components/article/List';
import ArticleCreate from '../components/article/Create';
import ArticleUpdate from '../components/article/Update';
import ArticleShow from '../components/article/Show';

export default [
  { name: 'ArticleList', path: '/articles/', component: ArticleList },
  { name: 'ArticleCreate', path: '/articles/create', component: ArticleCreate },
  {
    name: 'ArticleUpdate',
    path: '/articles/edit/:id',
    component: ArticleUpdate,
  },
  { name: 'ArticleShow', path: '/articles/show/:id', component: ArticleShow },
];
