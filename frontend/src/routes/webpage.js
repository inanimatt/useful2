import WebPageList from '../components/webpage/List';
import WebPageCreate from '../components/webpage/Create';
import WebPageUpdate from '../components/webpage/Update';
import WebPageShow from '../components/webpage/Show';

export default [
  { name: 'WebPageList', path: '/web_pages/', component: WebPageList },
  {
    name: 'WebPageCreate',
    path: '/web_pages/create',
    component: WebPageCreate,
  },
  {
    name: 'WebPageUpdate',
    path: '/web_pages/edit/:id',
    component: WebPageUpdate,
  },
  { name: 'WebPageShow', path: '/web_pages/show/:id', component: WebPageShow },
];
