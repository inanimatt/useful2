import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import articleRoutes from '../routes/article';
import webpageRoutes from '../routes/webpage';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
    },
    ...articleRoutes,
    ...webpageRoutes,
  ],
});
