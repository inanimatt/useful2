<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A user
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends Person implements UserInterface, \Serializable
{

    /**
     * @var string Username
     *
     * @ORM\Column(unique=true)
     * @Groups({"read", "write"})
     * @Assert\NotBlank
     */
    public $username;

    /**
     * @var array Roles
     *
     * @ORM\Column(type="json_array")
     * @Groups({"read", "write"})
     */
    public $roles = ['ROLE_USER'];

    public function __construct() {
    }

    public function setUsername($username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setPassword($password): self
    {
        return $this;
    }

    public function getPassword(): ?string
    {
        return '';
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
        ]);
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
        ) = unserialize($serialized);
    }
}
