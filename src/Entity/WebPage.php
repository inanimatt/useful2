<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A web page. Every web page is implicitly assumed to be declared to be of type WebPage, so the various properties about that webpage, such as `breadcrumb` may be used. We recommend explicit declaration if these properties are specified, but if they are found outside of an itemscope, they will be assumed to be about the page.
 *
 * @see http://schema.org/WebPage Documentation on Schema.org
 *
 * @ORM\Entity
 */
class WebPage
{
    /**
     * @var string|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string URL of the item
     *
     * @ORM\Column(type="text")
     * @Assert\Url
     * @Assert\NotNull
     */
    private $url;

    /**
     * @var string|null headline of the article
     *
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Type(type="string")
     */
    private $headline;

    /**
     * @var Person|null specifies the Person that is legally accountable for the CreativeWork
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $accountablePerson;

    /**
     * @var \DateTimeInterface the date on which the CreativeWork was created or the item was added to a DataFeed
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     * @Assert\NotNull
     */
    private $dateCreated;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setHeadline(?string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    public function setAccountablePerson(?Person $accountablePerson): self
    {
        $this->accountablePerson = $accountablePerson;

        return $this;
    }

    public function getAccountablePerson(): ?Person
    {
        return $this->accountablePerson;
    }

    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateCreated(): \DateTimeInterface
    {
        return $this->dateCreated;
    }
}
