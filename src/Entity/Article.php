<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An article, such as a news article or piece of investigative report. Newspapers and magazines have articles of many different types and this is intended to cover them all.\\n\\nSee also \[blog post\](http://blog.schema.org/2014/09/schemaorg-support-for-bibliographic\_2.html).
 *
 * @see http://schema.org/Article Documentation on Schema.org
 *
 * @ORM\Entity
 */
class Article
{
    /**
     * @var string|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null the actual body of the article
     *
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Type(type="string")
     */
    private $articleBody;

    /**
     * @var Person|null specifies the Person that is legally accountable for the CreativeWork
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $accountablePerson;

    /**
     * @var \DateTimeInterface the date on which the CreativeWork was created or the item was added to a DataFeed
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     * @Assert\NotNull
     */
    private $dateCreated;

    /**
     * @var \DateTimeInterface|null the date on which the CreativeWork was most recently modified or when the item's entry was modified within a DataFeed
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime
     */
    private $dateModified;

    /**
     * @var string headline of the article
     *
     * @ORM\Column(type="text")
     * @Assert\Type(type="string")
     * @Assert\NotNull
     */
    private $headline;

    /**
     * @var int the version of the CreativeWork embodied by a specified resource
     *
     * @ORM\Column(type="integer")
     * @Assert\Type(type="integer")
     * @Assert\NotNull
     */
    private $version;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setArticleBody(?string $articleBody): self
    {
        $this->articleBody = $articleBody;

        return $this;
    }

    public function getArticleBody(): ?string
    {
        return $this->articleBody;
    }

    public function setAccountablePerson(?Person $accountablePerson): self
    {
        $this->accountablePerson = $accountablePerson;

        return $this;
    }

    public function getAccountablePerson(): ?Person
    {
        return $this->accountablePerson;
    }

    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateCreated(): \DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateModified(?\DateTimeInterface $dateModified): self
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    public function getDateModified(): ?\DateTimeInterface
    {
        return $this->dateModified;
    }

    public function setHeadline(string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getHeadline(): string
    {
        return $this->headline;
    }

    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVersion(): int
    {
        return $this->version;
    }
}
