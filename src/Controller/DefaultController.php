<?php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController
{
    /**
     * @Route("/", name="homepage")
     */
    public function defaultAction(Request $request)
    {
        return new JsonResponse([
            'status' => 'ok',
            'statusCode' => 200,
        ]);
    }
}
