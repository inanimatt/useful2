<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180227131349 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE web_page (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', url LONGTEXT NOT NULL, headline LONGTEXT DEFAULT NULL, date_created DATETIME NOT NULL, accountable_person_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_D008BBD51D066DCF (accountable_person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD name LONGTEXT DEFAULT NULL, CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE article ADD accountable_person_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD article_body LONGTEXT DEFAULT NULL, ADD date_created DATETIME NOT NULL, ADD date_modified DATETIME DEFAULT NULL, ADD version INT NOT NULL, DROP title, DROP author, CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', CHANGE content headline LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E661D066DCF FOREIGN KEY (accountable_person_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_23A0E661D066DCF ON article (accountable_person_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE web_page');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E661D066DCF');
        $this->addSql('DROP INDEX IDX_23A0E661D066DCF ON article');
        $this->addSql('ALTER TABLE article ADD title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD author VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP accountable_person_id, DROP article_body, DROP date_created, DROP date_modified, DROP version, CHANGE id id VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci, CHANGE headline content LONGTEXT NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE user DROP name, CHANGE id id VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
