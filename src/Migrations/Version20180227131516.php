<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180227131516 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE web_page CHANGE accountable_person_id accountable_person_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE web_page ADD CONSTRAINT FK_D008BBD51D066DCF FOREIGN KEY (accountable_person_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article CHANGE accountable_person_id accountable_person_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE date_modified date_modified DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article CHANGE accountable_person_id accountable_person_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE date_modified date_modified DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE web_page DROP FOREIGN KEY FK_D008BBD51D066DCF');
        $this->addSql('ALTER TABLE web_page CHANGE accountable_person_id accountable_person_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8_unicode_ci COMMENT \'(DC2Type:guid)\'');
    }
}
