<?php
namespace App\Service;

use Psr\SimpleCache\CacheInterface;

class OICConfiguration
{
    protected $cache;
    protected $clientId;
    protected $clientSecret;
    protected $tenant;

    function __construct(CacheInterface $cache, $tenant, $clientId, $clientSecret) {
        $this->cache = $cache;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->tenant = $tenant;
    }

    public function getConfiguration()
    {
        $configuration = $this->cache->get('oidc.configuration');

        if (is_null($configuration)) {
            $url = 'https://accounts.google.com/.well-known/openid-configuration';
            $configuration = json_decode(file_get_contents($url), true);
            $this->cache->set('oidc.configuration', $configuration, 86400);
        }

        if (!is_array($configuration) || !isset($configuration['token_endpoint'])) {
            throw new \RuntimeException('Unable to retrieve openid connect configuration');
        }

        return $configuration;
    }

    public function getKeys()
    {
        $keys = $this->cache->get('oidc.keys');

        if (is_null($keys)) {
            $configuration = $this->getConfiguration();
            $url = 'https://www.googleapis.com/oauth2/v1/certs';
            $response = json_decode(file_get_contents($url), true);
            $keys = [];
            foreach ($response as $id => $key) {
                $keys[$id] = $key;
            }
            $this->cache->set('oidc.keys', $keys, 86400);
        }

        if (!is_array($keys)) {
            throw new \RuntimeException('Unable to retrieve signing keys');
        }

        return $keys;
    }

    public function getKey($kid)
    {
        $keys = $this->getKeys();

        foreach ($keys['keys'] as $key) {
            if ($key['kid'] === $kid) {
                return $key;
            }
        }

        throw new \Exception('No such key');
    }

    public function getTokenEndpoint()
    {
        $config = $this->getConfiguration();

        return $config['token_endpoint'];
    }

    public function getAuthorizationEndpoint()
    {
        $config = $this->getConfiguration();

        return $config['authorization_endpoint'];
    }

    public function getUserInfoEndpoint()
    {
        $host = $this->getGraphHostname();

        return sprintf('https://%s/v1.0/me', $host);
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    public function getIssuer()
    {
        $config = $this->getConfiguration();

        return $config['issuer'];
    }

    public function getTenant()
    {
        return $this->tenant;
    }
}
