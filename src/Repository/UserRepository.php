<?php
namespace App\Repository;

use App\Entity;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function create($username)
    {
        $user = new Entity\User();
        $user->setUsername($username);
        $this->save($user);

        return $user;
    }

    public function save(Entity\User $user)
    {
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }
}
