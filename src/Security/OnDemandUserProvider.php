<?php
namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OnDemandUserProvider implements UserProviderInterface
{
    private $repository;
    private $logger;

    public function __construct(EntityRepository $repository, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->repository->findOneByUsername($username);

        // User not found, create.
        if (null === $user) {
            $user = $this->repository->create($username);
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (null === $refreshedUser = $this->repository->findOneByUsername($user->getUsername())) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', json_encode($user->getId())));
        }

        return $refreshedUser;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return $class === 'App\Entity\User' || is_subclass_of($class, 'App\Entity\User');
    }

    public function updateProfile(User $user, $payload)
    {
        $user->setEmail($payload['email']);
        $user->setName($payload['name']);
        $this->repository->save($user);
    }
}
