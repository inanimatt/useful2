<?php
namespace App\Security;

use App\Entity\User;
use App\Exception\InvalidJWTException;
use App\Service\OICConfiguration;
use Doctrine\ORM\EntityRepository;
use Firebase\JWT\JWT;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

final class OICAuthenticator extends AbstractGuardAuthenticator
{
    protected $config;
    protected $router;
    protected $logger;
    protected $userRepository;

    public function __construct(OICConfiguration $config, RouterInterface $router, LoggerInterface $logger, EntityRepository $userRepository)
    {
        $this->config = $config;
        $this->router = $router;
        $this->logger = $logger;
        $this->userRepository = $userRepository;
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array). If you return null, authentication
     * will be skipped.
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * For example, for a form login, you might:
     *
     *      if ($request->request->has('_username')) {
     *          return array(
     *              'username' => $request->request->get('_username'),
     *              'password' => $request->request->get('_password'),
     *          );
     *      } else {
     *          return;
     *      }
     *
     * Or for an API token that's on a header, you might use:
     *
     *      return array('api_key' => $request->headers->get('X-API-TOKEN'));
     *
     * @param Request $request
     *
     * @return mixed|null
     */
    public function getCredentials(Request $request)
    {
        if (preg_match('/Bearer (.*)/', $request->headers->get('Authorization', ''), $matches)) {
            return [
                'type' => 'api',
                'id_token' => $matches[1],
            ];
        }

        if ($request->getSession()->has('username')) {
            $this->logger->debug('Authenticated session found. User: ' . $request->getSession()->get('username'));
            return [
                'type' => 'authenticated',
                'username' => $request->getSession()->get('username'),
            ];
        }

        if ($request->attributes->get('_route') === 'login_check') {
            if (!$request->request->has('id_token')) {
                throw new CustomUserMessageAuthenticationException('Missing parameters');
            }

            $this->logger->debug('Beginning validation process');
            // Validate state
            if ($request->getSession()->get('state') !== $request->request->get('state')) {
                $this->logger->error('State query parameter does not match session');
                throw new CustomUserMessageAuthenticationException('Invalid state');
            }

            $nonce = $request->getSession()->get('nonce');
            $token = $request->request->get('id_token');

            $request->getSession()->set('nonce', null); // Important: reset nonce to prevent replays
            $request->getSession()->set('id_token', $token);

            return [
                'type' => 'login',
                'nonce' => $nonce,
                'id_token' => $token,
            ];
        }

        return null;
    }


    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param mixed                 $credentials
     * @param UserProviderInterface $userProvider
     *
     * @throws AuthenticationException
     *
     * @return UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if ($credentials['type'] === 'login') {
            $payload = $this->decodeAndValidateIdToken($credentials, true);
            $credentials['payload'] = $payload;
            $credentials['username'] = $payload['sub'];
        }

        if ($credentials['type'] === 'api') {
            $payload = $this->decodeAndValidateIdToken($credentials, false);
            $credentials['payload'] = $payload;
            $credentials['username'] = $payload['sub'];
        }

        $user = $userProvider->loadUserByUsername($credentials['username']);

        // On initial login only (as opposed to loaded from session), update profile
        if ($credentials['type'] === 'login') {
            $userProvider->updateProfile($user, $payload);
        }

        return $user;
    }

    protected function decodeAndValidateIdToken($credentials, $checkNonce = true)
    {
        try {
            JWT::$leeway = 60;
            $payload = (array) JWT::decode($credentials['id_token'], $this->config->getKeys(), ['RS256']);
        } catch (\Exception $e) {
            throw new CustomUserMessageAuthenticationException('Login server returned an invalid token', [], 0, $e);
        }

        if ($payload['hd'] !== $this->config->getTenant()) {
            throw new CustomUserMessageAuthenticationException('Your Google account does not have access to this service');
        }

        if ($payload['iss'] !== $this->config->getIssuer()) {
            throw new CustomUserMessageAuthenticationException('Invalid issuer');
        }

        if (
            (is_string($payload['aud']) && $payload['aud'] !== $this->config->getClientId())
            || (is_array($payload['aud']) && !in_array($this->config->getClientId(), $payload['aud']))
        ) {
            throw new CustomUserMessageAuthenticationException('Invalid audience');
        }

        if ($checkNonce && $payload['nonce'] !== $credentials['nonce']) {
            throw new CustomUserMessageAuthenticationException('Token already used');
        }

        return $payload;
    }

    protected function updateProfile(User $user, $token)
    {
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->config->getUserInfoEndpoint(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Accept: application/json, application/jwt',
                'Authorization: Bearer ' . $token,
            ],
        ]);
        $result = curl_exec($ch);
        $this->logger->debug('AAD userinfo response', [
            'curlError' => curl_error($ch),
            'curlErrno' => curl_errno($ch),
            'curlInfo' => curl_getinfo($ch),
        ]);
        curl_close($ch);

        if ($result === false) {
            throw new CustomUserMessageAuthenticationException('Windows login services info endpoint failed to return a response');
        }

        $data = json_decode($result, true);
        dump($result);
        die($result);
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail. You may also throw an AuthenticationException if you wish
     * to cause authentication to fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return bool
     *
     * @throws AuthenticationException
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $message = 'Invalid Credentials';
        if ($exception instanceof CustomUserMessageAuthenticationException) {
            $message = $exception->getMessageKey();
        }
        throw new HttpException(401, $message);
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * Examples:
     *  A) For a form login, you might redirect to the login page
     *      return new RedirectResponse('/login');
     *  B) For an API token authentication system, you return a 401 response
     *      return new Response('Auth header required', 401);
     *
     * @param Request                 $request       The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        // state is returned during auth response to prevent CSRF
        $state = $this->getRandom();

        // nonce is returned in id_token to prevent replay attack
        $nonce = $this->getRandom();

        $session = $request->getSession();
        $session->set('state', $state);
        $session->set('nonce', $nonce);
        $session->set('target', $request->getUri());
        $redirectUri = str_replace('http://', 'https://', $this->router->generate('login_check', [], UrlGeneratorInterface::ABSOLUTE_URL));

        $query = http_build_query([
            'response_type' => 'id_token',
            'response_mode' => 'form_post',
            'client_id' => $this->config->getClientId(),
            'redirect_uri' => $redirectUri,
            'scope' => 'openid profile email',
            'state' => $state,
            'nonce' => $nonce,
            'prompt' => 'login',
            'hd' => $this->config->getTenant(),
        ]);

        $url = vsprintf('%s?%s', [
            $this->config->getAuthorizationEndpoint(),
            $query,
        ]);

        return new RedirectResponse($url);
    }

    protected function getRandom()
    {
        return rtrim(strtr(base64_encode(random_bytes(24)), '+/', '-_'), '=');
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if (!$request->getSession()->has('username')) {
            $target = $request->getSession()->get('target', $this->router->generate('homepage'));
            $request->getSession()->set('username', $token->getUser()->getUsername());

            $response = new RedirectResponse($target);
            $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie(
                'token',
                $request->getSession()->get('id_token'),
                0, // Expire at end of session
                '/', // Path
                null, // Domain
                true, // Secure
                false, // not HttpOnly - since making it available to JS is the point
                false, // not raw
                \Symfony\Component\HttpFoundation\Cookie::SAMESITE_LAX
            ));

            return $response;
        }
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function supports(Request $request)
    {
        if ($request->getSession()->has('username')) {
            return true;
        }
        
        if ($request->isMethod('POST') && $request->attributes->get('_route') === 'login_check') {
            return true;
        }

        if (strpos($request->headers->get('Authorization', ''), 'Bearer ') !== false) {
            return true;
        }

        return false;
    }
}
